package lambdas;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Java program to find frecuency of  words in a file.
 *
 * @author
 */
public class Lambdas2 {
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Map<String, String> m = replaceWith();
		Path filePath = Paths.get("cp.txt");
		List<String> wordList = null;
		
		try (Stream<String> lines = Files.lines(filePath)) {			  
			  Stream<String> words = lines
			    .flatMap(line -> Stream.of(line.split("[^[\\wáéíóúñ]]")));
			  
			  wordList = words.collect(Collectors.toList());

			  System.out.println(wordList.size());
		} catch (IOException e) {

			e.printStackTrace();
		}
		
		Map<String, Long> frequencyMap = wordList.stream()
				  .map(String::toLowerCase).sorted().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
	  
        System.out.println("List of repeated word from file and their count");
        String format = "%-20s%s%n";
        frequencyMap.forEach((k,v) -> System.out.printf(format, v, k));
        
        
        
        


			  
//		try {
//			Stream<String> lines = Files.lines(filePath, Charset.forName("UTF-8"));
//			List<String> replacedLine = lines.map(line -> replaceWords(line, m)).collect(Collectors.toList());
//			Files.write(filePath, replacedLine, Charset.forName("UTF-8"));
//			lines.close();
//			System.out.println("Done");
//		} 


	}

	public static Map<String, String> replaceWith() {

		Map<String, String> map = new HashMap<>();
		map.put("Blue", "Black");
		map.put("2014", "2016");

		return map;

	}

	public static String replaceWords(String str, Map<String, String> map) {
		for (Map.Entry<String, String> entry : map.entrySet()) {

			if (str.contains(entry.getKey())) {
				str = str.replace(entry.getKey(), entry.getValue());
			}
		}
		return str;
	}

}